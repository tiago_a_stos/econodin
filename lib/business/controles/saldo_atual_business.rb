# encoding: UTF-8

require 'utils/currency'

class SaldoAtualBusiness
	@@currency = Currency.new

	def atualizar(valor, current_user)
		p "Utilizando a classe saldo_atual_business"
		saldos = SaldoAtual.where(:user => current_user)
		saldo = saldos[0]
		
		if saldo == nil
			saldo = SaldoAtual.new
			saldo.valor = valor
			saldo.user = current_user
			saldo.data_criacao = Date.new
			
		else
			saldo.valor = valor
		end

		saldo.data_alteracao = Date.new

		saldo.save!

		@@currency.to_currency saldo.valor
	end

end
