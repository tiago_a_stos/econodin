# encoding: UTF-8

require 'utils/currency'

class LancamentoBusiness
	@@currency = Currency.new

	def criar(lancamento, recorrencia, current_user)
		#verifica se eh recorrencia ou lançamento normal
		p '>>> Criando Lancamento'

		if recorrencia.ativado == 'on'
			p '>>> Lancamento Recorrente'
			recorrencia.user = current_user
			recorrencia.descricao = lancamento.descricao
			recorrencia.save!
			gera_recorrencia( lancamento, recorrencia, current_user )
			recorrencia.save!
		else
			lancamento.save!
		end
	end

	#protected

	def gera_recorrencia(lancamento, recorrencia, current_user)
		recorrencias = Array.new

		if recorrencia.parcelado == 'on'
			p '>>> Recorrencia por parcelas'
			lancamentos = trata_recorrencia_parcelada( lancamento, recorrencia, current_user )

		else
			p '>>> Recorrencia por data'
			lancamentos = trata_recorrencia_por_data( lancamento, recorrencia, current_user )
		end

		lancamentos
	end

	def trata_recorrencia_parcelada(lancamento, recorrencia, current_user)
		numero_parcelas = recorrencia.quantidade_parcelas
		parcela_inicial = recorrencia.parcela_inicial - 1
		descricao = lancamento.descricao
		lancamentos = Array.new

		num = 0;

		(parcela_inicial...numero_parcelas).each { |numero|
			data_lancamento = define_data_conforme_peridiocidade(recorrencia.periodicidade, lancamento.data, num)
			lancamento.descricao = descricao + " " + (numero+1).to_s + "/" + numero_parcelas.to_s
			lancamento_recorrente = geraLancamento(data_lancamento, lancamento, recorrencia, current_user)
			lancamentos << lancamento_recorrente
			num += 1;
		}

		lancamentos
	end

	def trata_recorrencia_por_data( lancamento, recorrencia, current_user )
		data_inicial = lancamento.data
		data_final = recorrencia.data_fim
		data_lancamento = data_inicial

		data_final.change(:offset => '-3')

		lancamentos = Array.new

		cont = 0
		while data_lancamento <= data_final
			lancamento_recorrente = geraLancamento( data_lancamento, lancamento, recorrencia, current_user )
			lancamentos << lancamento_recorrente

			cont += 1
			data_lancamento = define_data_conforme_peridiocidade( recorrencia.periodicidade, lancamento.data, cont )
			p '-->>> dt Lancto: ' + data_lancamento.to_s
			p '--->>> dt final: ' + data_final.to_s
			p '--->> dt menor ou igual? ' + (data_lancamento <= data_final).to_s
		end

		lancamentos
	end

	def geraLancamento( data_lancamento, lancamento, recorrencia, current_user )
		lancamento_recorrente = Lancamento.new
			
		lancamento_recorrente.data = data_lancamento
		lancamento_recorrente.descricao = lancamento.descricao
		lancamento_recorrente.valor_previsto = lancamento.valor_previsto
		lancamento_recorrente.user = current_user
		lancamento_recorrente.recorrencia = recorrencia
		lancamento_recorrente.save!
			
		recorrencia.lancamentos << lancamento_recorrente

		lancamento_recorrente
	end

	def define_data_conforme_peridiocidade( periodicidade, data, numero )
		case periodicidade
		when 'Diário'
			data_lancamento = data + numero

		when 'Semanal'
			data_lancamento = data + (numero * 7)

		when 'Quinzenal'
			data_lancamento = data + (numero * 14)

		when 'Mensal'
			data_lancamento = data >> numero

		when 'Bimestral'
			data_lancamento = data >> (numero * 2)

		when 'Semestral'
			data_lancamento = data >> (numero * 6)

		when 'Anual'
			data_lancamento = data >> (numero * 12)
		end

		data_lancamento.midnight
	end

end