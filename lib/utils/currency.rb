#ecncoding: UTF-8

class Currency
  def string_to_int(valor)
    valor.gsub(",", "").to_i
  end

  def to_currency(valor)
    if valor >= 0
      converte_valor_positivo valor
    else
      converte_valor_negativo valor
    end
  end

  private 

  def converte_valor_positivo(valor)
    str_valor = valor.to_s

    if str_valor.size == 1
      str_valor = "0,0" + str_valor

    elsif str_valor.size == 2
      str_valor = "0," + str_valor

    else 
      real = valor / 100
      centavos = valor % 100

      if centavos < 10
        centavos = "0" + centavos.to_s
      else
        centavos = centavos.to_s
      end

      real.to_s + "," + centavos
    end
  end

  def converte_valor_negativo(valor)
    valor *= -1
    valor1 = converte_valor_positivo valor
    valor1 = "-" + valor1
    valor1
  end

end