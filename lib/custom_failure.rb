#encoding: UTF-8
class CustomFailure < Devise::FailureApp
  
  def redirect_url
    headers = {}
    if warden_options[:scope] == :user
      root_path :f => 0
    else
      root_path :f => 0
    end
  end

  def respond
    if http_auth?
      http_auth
    else
      redirect
    end
  end
end