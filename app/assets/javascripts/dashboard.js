var econodinApp = angular.module('econodin', ['templates', 'ngTagsInput', 'cgBusy']);

function DashboardController($scope, $http) {

	$scope.tags = [
    { text: 'just' },
    { text: 'some' },
    { text: 'cool' },
    { text: 'tags' }
  ];

  $scope.loadTags = function(query) {
  	return $http.get('/tags?query=' + query);
  };

	$scope.saldo_atual = "0,00";
	$scope.exibir_finalizados = false;

	$("#valor_previsto").maskMoney({thousands:'', decimal:',', allowNegative: true});
	$("#valor_realizado").maskMoney({thousands:'', decimal:',', allowNegative: true});
	$(".saldo-atual").maskMoney({thousands:'', decimal:',', allowNegative: true});

	$("#msg_saldo_atual").hide();

	$( "#datepicker" ).datepicker( $.datepicker.regional[ "pt-BR" ] );
	$( "#data" ).datepicker();
	$( "#recorrencia_data_fim" ).datepicker();

	$(".recorrencia, .parcelas").prop( "disabled", true );
	$("#recorrencia_opcao").change( function() {
		if ( $(this).is(":checked") ) {
			$scope.habilita_campos_recorrencia();
		} else {
			$scope.desabilita_campos_recorrencia();
		}
	});

	$("#opcao_parcelas").change( function() {
		if ( $(this).is(":checked") ) {
			$scope.habilita_campos_parcelas();
			$("#recorrencia_data_fim").prop("disabled", true);
			$("#recorrencia_data_fim").prop("required", false);
		} else {
			$scope.desabilita_campos_parcelas();
			$("#recorrencia_data_fim").prop("disabled", false);
			$("#recorrencia_data_fim").prop("required", true);
		}
	});

	$('#fechar_opcoes').on('click', function() {
		$("#modal-lancamento").modal('hide');
	});

	$scope.habilita_campos_recorrencia = function() {
		$(".recorrencia").prop("disabled", false);
		$(".recorrencia").prop("required", true);
		$("#opcao_parcelas").prop("disabled", false);
	}

	$scope.desabilita_campos_recorrencia = function() {
		$(".recorrencia, .parcelas").prop("disabled", true);
		$("#opcao_parcelas").prop("checked", false);
		$("#opcao_parcelas").prop("disabled", true);
		$(".recorrencia").prop("required", false);
	}

	$scope.desabilita_campos_parcelas = function() {
		$(".parcelas").prop("disabled", true);
		$(".parcelas").prop("required", false);
	}

	$scope.habilita_campos_parcelas = function() {
		$(".parcelas").prop("disabled", false);
		$(".parcelas").prop("required", true);
	}

	$scope.busca_saldo_atual = function() {
		$http.get( '/saldoatual' ).success( function( saldo ) {
			$scope.preenche_saldo_atual( saldo );
		});
	}

	$scope.preenche_saldo_atual = function( retorno ) {
		console.log("Preenche saldo atual");
		console.log(retorno);
		if ( retorno !== null ) {
			$scope.saldo_atual = retorno.valor;
		} else {
			$scope.saldo_atual = "0,00";
		}

		console.log("Saldo Atual: " + $scope.saldo_atual);
	}

	$scope.submit_saldo_atual = function(event) {
		event.preventDefault();
		console.log("Teste http.put" + $scope.saldo_atual);

		$http.put( "/saldoatual", {valor: $scope.saldo_atual} );

		/*$(".form_saldo_atual").submit(
			function(retorno) {
				console.log("Atualizando saldo atual");
				var options = {success: function( retorno ) {
					$scope.preenche_saldo_atual( retorno );
				}};
				$(this).ajaxSubmit(options);
				return false;
			}
		);*/
	}

	$scope.busca_lancamentos = function() {
		console.log( $scope.exibir_finalizados );
		$http.get(
			'/lancamentos', {
				params: { mes_lancamentos: $scope.mes_selecionado, exibir_finalizados: $scope.exibir_finalizados }
			}
		).success( function( retorno ) {
			$scope.atualiza_lancamentos( retorno );
		});
	}

	$scope.atualiza_lancamentos = function( retornos ) {
		$(".corpo_tabela").empty();

		$scope.lancamentos = retornos;
		console.log( JSON.parse(JSON.stringify($scope.lancamentos)) );

		$scope.limpa_formulario_lancamento();
	}

	$scope.abre_formulario_lancamento = function() {
		$scope.titulo = "Novo Lançamento";
		$scope.acao = "cadastrar";

		$("#div_valor_realizado").hide();
		$("#div_opt_recorrencia").show();
		$("#frm_recorrencia").show();

		$("#modal-lancamento").modal('show');
	}

	$scope.define_classe_valor = function( valor ) {
		var classe_valor = "text-primary";

		if (valor == null) return classe_valor;

		var valor_f = parseFloat( valor.replace(",", ".") );
		if (valor_f < 0) {
			classe_valor = "text-danger";
		}
		return classe_valor;
	}

	$scope.teste = function() {
		alert("Teste");
	}


	$scope.limpa_formulario_lancamento = function() {
		$scope.oid = "";
		$scope.acao = "";
		$scope.data_lancamento = "";
		$scope.description = "";
		$scope.valor_previsto = "";
		$scope.valor_realizado = "";
		$scope.recorrencia_data_fim = "";
		$('#recorrencia_periodicidade').val( "0" );

		$('#recorrencia_opcao').attr( "disabled", false );
		$("#modal-lancamento").modal('hide');
		$("#recorrencia_opcao").prop("checked", false);

		$scope.desabilita_campos_recorrencia();
		$scope.desabilita_campos_parcelas();

	}

	$scope.finaliza_lancamento = function( oid ) {
		$scope.oid = oid;
		$scope.acao = "finalizar";

		$http.get( '/lancamento/' + oid ).success( function( retorno ) {
			$scope.abre_formulario_finalizacao( retorno );
		});
	}

	$scope.abre_formulario_finalizacao = function( retorno ) {
		$scope.titulo = "Finaliza Lançamento";

		$scope.preenche_formulario_lancamento( retorno );
		$("#div_valor_realizado").show();
		$("#div_opt_recorrencia").hide();
		$("#frm_recorrencia").hide();
		$("#modal-lancamento").modal('show');
		$scope.valor_realizado = $scope.valor_previsto;
	}

	$scope.open_balance_form = function() {
		$("#form-balance").modal('show');
	}

	$scope.open_month_selector_form = function() {
		$("#form-month-selector").modal('show');
	}

	$scope.preenche_formulario_lancamento = function(lancamento) {
		$scope.data_lancamento = lancamento.data;
		$scope.description = lancamento.descricao;
		$scope.valor_previsto = lancamento.valor_previsto;
	}

	$scope.edita_lancamento = function( oid ) {
		$scope.oid = oid;
		$scope.acao = "editar";

		$http.get( '/lancamento/' + oid ).success( function( retorno ) {
			$scope.abre_formulario_edicao(retorno);
		});
	}

	$scope.open_exclude_dialog = function( oid ) {
		$scope.oid_exclude = oid;
		$("#exclude_alert").modal('show');
	}

	$scope.close_exclude_dialog = function() {
		$("#exclude_alert").modal('hide');
	}

	$scope.exclude_launch = function() {
		$.ajax({
			type: "DELETE",
			url: "/lancamento/" + $scope.oid_exclude,
			dataType: "json",
			success: $scope.retorno_exclusao
		});
	}

	$scope.retorno_exclusao = function( retorno ) {
		if ( retorno.resp == "RECURRENCE") {
			console.log( "excluir recorrencia" );
		}
		$("#exclude_alert").modal('hide');
		$scope.busca_lancamentos(retorno);
	}


	$scope.submit_lancamento = function() {
		$("#form_lancamento").submit( function() {
			var action = "";
			var metodo = "";

			if ( $scope.acao == "cadastrar" ) {
				action = "/lancamento";
				metodo = "POST";
			} else if ( $scope.acao == "editar" ) {
				action = "/lancamento/" + $scope.oid;
				metodo = "PATCH";
			} else if ( $scope.acao == "finalizar" ) {
				action = "/lancamento/" + $scope.oid + "/finalizar";
				metodo = "PATCH";
			}

			var options = {
				url: action,
				type: metodo,
				success: $scope.acao_apos_submit_lancamento
			};

			$(this).ajaxSubmit(options);

			return false;
		});
	}

	$scope.acao_apos_submit_lancamento = function() {
		$scope.busca_saldo_atual();
		$scope.busca_lancamentos();

		if ($scope.acao == "finalizar") {
			$("#msg_saldo_atual").show();
			var callback = function () {
				$("#msg_saldo_atual").hide()
			};
			setTimeout(callback, 5000);
		}
	}


	$scope.abre_formulario_edicao = function (retorno) {
		$scope.titulo = "Edita Lançamento";
		$scope.preenche_formulario_lancamento(retorno);
		$("#div_valor_realizado").hide();
		$("#div_opt_recorrencia").show();
		$("#frm_recorrencia").show();
		$("#modal-lancamento").modal('show');
	}




	var dataAtual = new Date();
	dataAtual.setDate(1);
	$scope.mes_selecionado = (dataAtual.getMonth() + 1) + "/" + dataAtual.getFullYear();

	$scope.mes_anterior = function() {
		var mes = dataAtual.getMonth();
		mes = mes -1;
		dataAtual.setMonth( mes );
		$scope.mes_selecionado = (dataAtual.getMonth() + 1) + "/" + dataAtual.getFullYear();
		$scope.busca_lancamentos();
	}

	$scope.mes_proximo = function() {
		var mes = dataAtual.getMonth();
		mes = mes +1;
		dataAtual.setMonth( mes );
		$scope.mes_selecionado = (dataAtual.getMonth() + 1) + "/" + dataAtual.getFullYear();
		$scope.busca_lancamentos();
	}

	$scope.busca_lancamentos();
	$scope.submit_lancamento();



	$(function () {
	    $('.button-checkbox').each(function () {
	        // Settings
	        var $widget = $(this),
	            $button = $widget.find('button'),
	            $checkbox = $widget.find('input:checkbox'),
	            color = $button.data('color'),
	            settings = {
	                on: { icon: 'glyphicon glyphicon-check' },
	                off: { icon: 'glyphicon glyphicon-unchecked' }
	            };

	        // Event Handlers
	        $button.on('click', function () {
						$scope.exibir_finalizados = !$checkbox.is(':checked');
	            $checkbox.prop('checked', !$checkbox.is(':checked'));
	            $checkbox.triggerHandler('change');
	            updateDisplay();
	        });
	        $checkbox.on('change', function () {
						$scope.busca_lancamentos();
	          updateDisplay();
	        });

	        // Actions
	        function updateDisplay() {
	            var isChecked = $checkbox.is(':checked');
	            // Set the button's state
	            $button.data('state', (isChecked) ? "on" : "off");
	            // Set the button's icon
	            $button.find('.state-icon')
	                .removeClass()
	                .addClass('state-icon ' + settings[$button.data('state')].icon);
	            // Update the button's color
	            if (isChecked) {
	                $button
	                    .removeClass('btn-default')
	                    .addClass('btn-' + color + ' active');
	            }
	            else {
	                $button
	                    .removeClass('btn-' + color + ' active')
	                    .addClass('btn-default');
	            }
	        }

	        // Initialization
	        function init() {
	            updateDisplay();
	        }
	        init();
	    });
	});
}

econodinApp
	.controller('DashboardController', ['$scope', '$http', DashboardController]);
