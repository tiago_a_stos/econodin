#encoding: UTF-8
require 'utils/currency'
require 'business/controles/saldo_atual_business'
require 'business/lancamento/lancamento_business'

class LancamentoController < ApplicationController
	@@currency = Currency.new

	def update
		id_objeto = params[:id]
		lancamento = Lancamento.find( id_objeto )

		lancamento.data = params[:lancamento][:data]
		lancamento.descricao = params[:lancamento][:descricao]
		lancamento.valor_previsto = @@currency.string_to_int params[:lancamento][:valor_previsto]

		lancamento.save!

		hash_lancamento = Hash.new
		hash_lancamento[:oid] = lancamento._id.to_s
		hash_lancamento[:data] = lancamento.data.to_s(:data_br)
		hash_lancamento[:descricao] = lancamento.descricao
		hash_lancamento[:valor_previsto] = @@currency.to_currency lancamento.valor_previsto
		hash_lancamento[:valor_realizado] = @@currency.to_currency lancamento.valor_realizado if lancamento.valor_realizado?

		render :json => hash_lancamento
	end

	def create
		lancamento_business = LancamentoBusiness.new

		lancamento = Lancamento.new(ad_params)
		lancamento.valor_previsto = @@currency.string_to_int params[:lancamento][:valor_previsto]
		lancamento.user = current_user

		begin
			recorrencia = Recorrencia.new(ad_params_recorrencia)
		rescue Exception => e
			puts e.message
			recorrencia = Recorrencia.new
		end

		lancamento_business.criar(lancamento, recorrencia, current_user)

		render :json => lancamento
	end

	def list
		mes_lancamentos = params[:mes_lancamentos]
		exibir_finalizados = params[:exibir_finalizados]
		p exibir_finalizados.class

		data_inicio = DateTime.parse mes_lancamentos
		data_fim = data_inicio.end_of_month

		lancamentos_anteriores = Lancamento.where(
			:user => current_user,
			:valor_realizado.in => ["", nil],
			:data.lt => data_inicio
		).order_by(:data => :asc)

		if exibir_finalizados == "true"
			lancamentos = Lancamento.where(
				:user => current_user,
				:data.gte => data_inicio,
				:data.lte => data_fim
			).order_by(:data => :asc)

		else
			lancamentos = Lancamento.where(
				:user => current_user,
				:valor_realizado.in => ["", nil],
				:data.gte => data_inicio,
				:data.lte => data_fim
			).order_by(:data => :asc)
		end

		array_lancamentos = Array.new

		saldos = SaldoAtual.where(:user => current_user)
		saldo = saldos[0]
		saldo_previsto = saldo.valor

		lancamentos_anteriores.each do |lancamento|
			saldo_previsto += lancamento.valor_previsto
		end

		lancamentos.each do |lancamento|
			hash_lancamento = Hash.new

			hash_lancamento[:oid] = lancamento._id.to_s
			hash_lancamento[:data] = lancamento.data
			hash_lancamento[:descricao] = lancamento.descricao

			if lancamento.valor_realizado?
				hash_lancamento[:valor_previsto] = @@currency.to_currency lancamento.valor_realizado
			else
				hash_lancamento[:valor_previsto] = @@currency.to_currency lancamento.valor_previsto
			end

			hash_lancamento[:valor_realizado] = @@currency.to_currency lancamento.valor_realizado if lancamento.valor_realizado?

			unless lancamento.valor_realizado?
				saldo_previsto += lancamento.valor_previsto
			end

			hash_lancamento[:saldo_previsto] = @@currency.to_currency saldo_previsto unless lancamento.valor_realizado?

			array_lancamentos << hash_lancamento
		end

		render :json => array_lancamentos
	end

	def destroy
		id_objeto = params[:id]
		lancamento = Lancamento.find( id_objeto )

		resposta = Hash.new

		# if lancamento.recorrencia?
		# 	p ">>>>>>>>>>>> Esse pagamento faz parte de recorrencia"
		# 	resposta[:resp] = "RECURRENCE"
		# else
		# 	p "<<<<<<<<<<<< Esse pagamento não faz parte de recorrencia"
		# end

		if lancamento.destroy
			resposta[:resp] = "SUCCESS"
		else
			resposta[:resp] = "FAIL"
		end

		render :json => resposta
	end

	def show
		id_objeto = params[:id]
		lancamento = Lancamento.find( id_objeto )

		hash_lancamento = Hash.new
		hash_lancamento[:oid] = lancamento._id.to_s
		hash_lancamento[:data] = lancamento.data.to_s(:data_br)
		hash_lancamento[:descricao] = lancamento.descricao
		hash_lancamento[:valor_previsto] = @@currency.to_currency lancamento.valor_previsto
		unless lancamento.valor_realizado == nil
			hash_lancamento[:valor_realizado] = @@currency.to_currency lancamento.valor_realizado
		end

		render :json => hash_lancamento
	end

	def finalizar
		id_objeto = params[:id]
		lancamento = Lancamento.find( id_objeto )

		lancamento.data = params[:lancamento][:data]
		lancamento.descricao = params[:lancamento][:descricao]
		lancamento.valor_previsto = @@currency.string_to_int params[:lancamento][:valor_previsto]
		lancamento.valor_realizado = @@currency.string_to_int params[:lancamento][:valor_realizado]

		lancamento.save!

		saldo_atual_business = SaldoAtualBusiness.new
		valor_realizado = calcula_valor_saldo_atual @@currency.string_to_int params[:lancamento][:valor_realizado]
		valor = saldo_atual_business.atualizar valor_realizado, current_user

		hash_lancamento = Hash.new
		hash_lancamento[:oid] = lancamento._id.to_s
		hash_lancamento[:data] = lancamento.data.to_s(:data_br)
		hash_lancamento[:descricao] = lancamento.descricao
		hash_lancamento[:valor_previsto] = @@currency.to_currency lancamento.valor_previsto
		hash_lancamento[:valor_realizado] = @@currency.to_currency lancamento.valor_realizado if lancamento.valor_realizado?

		render :json => hash_lancamento
	end

	private

	def calcula_valor_saldo_atual(valor_realizado)
		saldos = SaldoAtual.where(:user => current_user)
		saldo = saldos[0]
		saldo_atualizado = saldo.valor + valor_realizado

		saldo_atualizado
	end

	def ad_params
		params.require(:lancamento).permit(:data, :descricao, :valor_previsto)
	end

	def ad_params_recorrencia
		params.require(:recorrencia).permit(:ativado, :periodicidade, :parcelado, :quantidade_parcelas, :parcela_inicial, :data_fim)
	end
end
