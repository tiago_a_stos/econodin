#encoding: UTF-8

require 'utils/currency'
require 'business/controles/saldo_atual_business'

class SaldoAtualController < ApplicationController
	@@currency = Currency.new

	def show
		saldo = SaldoAtual.where(:user => current_user)

		p '------------------------'
		p saldo.count
		p '------------------------'

		if saldo.count == 0
			saldo_atual_business = SaldoAtualBusiness.new
			valor = saldo_atual_business.atualizar @@currency.string_to_int("0,00"), current_user

		else
			valor = @@currency.to_currency saldo[0].valor
		end

		retorno = Hash.new
		retorno[:valor] = valor

		render :json => retorno
	end

	def update
		saldo_atual_business = SaldoAtualBusiness.new

		valor = saldo_atual_business.atualizar @@currency.string_to_int(params[:valor]), current_user

		retorno = Hash.new
		retorno[:valor] = valor

		render :json => retorno
	end
end
