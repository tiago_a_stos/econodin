#encoding: UTF-8
class SaldoAtual
  include Mongoid::Document
  field :data_criacao, type: DateTime
  field :data_alteracao, type: DateTime
  field :valor, type: Integer
  belongs_to :user
end
