#encoding: UTF-8
class Lancamento
  include Mongoid::Document
  field :data, type: DateTime
  field :descricao, type: String
  field :valor_previsto, type: Integer
  field :valor_realizado, type: Integer
  belongs_to :user
  belongs_to :recorrencia
end
