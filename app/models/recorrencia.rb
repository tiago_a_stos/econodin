#encoding: UTF-8
class Recorrencia
  include Mongoid::Document
  field :descricao, type: String
  field :ativado, type: String
  field :periodicidade, type: String
  field :data_fim, type: DateTime
  field :parcelado, type: String
  field :quantidade_parcelas, type: Integer
  field :parcela_inicial, type: Integer
  belongs_to :user
  has_many :lancamentos
end