#Econodin::Application.routes.draw do
Rails.application.routes.draw do

  get 'investments/index'

	resources :lancamento, :only => [:create, :update, :destroy, :show]
	get "lancamentos", :controller => "lancamento", :action => "list"
	patch "lancamento/:id/finalizar", :controller => "lancamento", :action => "finalizar"

	resource :saldoatual, :controller => "saldo_atual", :only => [:update, :show]

	get "dashboard2" => "dashboard#index2"
	get "dashboard" => "dashboard#index"
	root "home#index"
	devise_for :users
end
