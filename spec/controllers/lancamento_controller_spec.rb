=begin
require 'spec_helper'

describe LancamentoController do

	before do 
		@user = User.create!(email: 'teste@tfdsf.ts1', password: '121242534')
	end

	describe "GET #show" do
		it "deve retornar um lancamento especifico" do
			lancamento = Lancamento.create!(data: '13/04/2014', descricao: 'Teste lançamento', valor_previsto: 12454, user: @user)

			hash_lancamento = Hash.new
			hash_lancamento[:oid] = lancamento._id.to_s
			hash_lancamento[:data] = lancamento.data.to_s(:data_br)
			hash_lancamento[:descricao] = lancamento.descricao
			hash_lancamento[:valor_previsto] = @@currency.to_currency lancamento.valor_previsto
			hash_lancamento[:valror_realizado] = @@currency.to_currency lancamento.valor_realizado if lancamento.valor_realizado?

			get :show, :id => lancamento._id.to_s
			response.body.should == hash_lancamento.to_json
		end

		it "deve retornar um lancamento especifico com valor realizado preenchido" do
			lancamento = Lancamento.create!(data: '13/04/2014', descricao: 'Teste lançamento', valor_previsto: 12454, valor_realizado: 1235, user: @user)

			hash_lancamento = Hash.new
			hash_lancamento[:oid] = lancamento._id.to_s
			hash_lancamento[:data] = lancamento.data.to_s(:data_br)
			hash_lancamento[:descricao] = "Teste lançamento"
			hash_lancamento[:valor_previsto] = "124,54"
			hash_lancamento[:valor_realizado] = "12,35"

			get :show, :id => lancamento._id.to_s
			response.body.should == hash_lancamento.to_json
		end
	end

	describe "PATCH #update" do
		include Devise::TestHelpers

		it "deve atualizar um lancamento" do
			lancamento = Lancamento.create!(data: '13/04/2014', descricao: 'Teste lançamento', valor_previsto: 12454, user: @user)
			
			hash_lancamento = Hash.new
			hash_lancamento[:oid] = lancamento._id.to_s
			hash_lancamento[:data] = "30/04/2014"
			hash_lancamento[:descricao] = "Testando update"
			hash_lancamento[:valor_previsto] = "12,34"
			hash_lancamento[:valror_realizado] = @@currency.to_currency lancamento.valor_realizado if lancamento.valor_realizado?

			controller.stub :current_user => @user

			patch :update, 
					:id => lancamento._id.to_s,
					lancamento: {
						data: "30/04/2014",
						descricao: "Testando update",
						valor_previsto: "12,34"
					}

			response.body.should == hash_lancamento.to_json
		end
	end

	describe "POST 'create'" do
		include Devise::TestHelpers

		it "deve realizar lancamento comum e retornar oid preenchido" do
			post :create,
					lancamento: {
						data: "19/04/2014",
						descricao: "Testando update",
						valor_previsto: "12,34"
					}

			resposta_json = JSON.parse( response.body )
			resposta_json["_id"].should_not be_nil
		end

		it "deve realizar lancamento com recorrencia por parcela" do
			post :create,
					lancamento: {
						data: "19/04/2014",
						descricao: "Testando recorrencia",
						valor_previsto: "12,34"
					},
					recorrencia: {
						ativado: "on",
						periodicidade: "Diário",
						data_fim: "",
						parcelado: "on",
						quantidade_parcelas: 5,
						parcela_inicial: 1
					}

			recorrencia = Recorrencia.find_by( descricao: "Testando recorrencia" )
			recorrencia.should_not be_nil

			recorrencia.lancamentos.size.should == 5
		end

		it "deve realizar lancamento com recorrencia por parcela iniciada em 3" do
			post :create,
					lancamento: {
						data: "19/04/2014",
						descricao: "Testando recorrencia",
						valor_previsto: "12,34"
					},
					recorrencia: {
						ativado: "on",
						periodicidade: "Mensal",
						data_fim: "",
						parcelado: "on",
						quantidade_parcelas: 5,
						parcela_inicial: 2
					}

			recorrencia = Recorrencia.find_by( descricao: "Testando recorrencia" )
			recorrencia.should_not be_nil

			recorrencia.lancamentos.size.should == 4
			recorrencia.lancamentos[3].descricao.should == "Testando recorrencia 5/5"
		end

		it "deve realizar lancamento com recorrencia por data" do
			post :create,
					lancamento: {
						data: "19/04/2014",
						descricao: "Testando recorrencia Semanal",
						valor_previsto: "12,34"
					},
					recorrencia: {
						ativado: "on",
						periodicidade: "Semanal",
						data_fim: "25/05/2014",
						parcelado: "off",
						quantidade_parcelas: 0,
						parcela_inicial: 0
					}

			recorrencia = Recorrencia.find_by( descricao: "Testando recorrencia Semanal" )
			recorrencia.should_not be_nil

			recorrencia.lancamentos.size.should == 6
		end
	end
end
=end