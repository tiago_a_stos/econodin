require 'spec_helper'
require 'business/lancamento/lancamento_business'
require 'utils/currency'

describe LancamentoBusiness do
	before do 
		@user = User.create!(email: 'teste@tfdsf.ts1', password: '121242534')
		@lancamento_business = LancamentoBusiness.new
	end

	it 'deve retornar array com lancamentos recorrentes diarios para recorrencia por parcelas' do
		recorrencia = prepara_objeto_recorrencia( 'Diário', 7, 'on', 1, '' )
		lancamento = prepara_objeto_lancamento
		recorrencias = @lancamento_business.gera_recorrencia( lancamento, recorrencia, @user )

		data_lacamento = DateTime.new(2014,4,19,0,0,0 )
		expect( recorrencias[0].data ).to eq( data_lacamento )

		data_lacamento = DateTime.new(2014,4,22,0,0,0 )
		expect( recorrencias[3].data ).to eq( data_lacamento )

		expect( recorrencias[1].descricao ).to eq('Teste de Recorrencia 2/7')
		expect( recorrencias[2].descricao ).to eq('Teste de Recorrencia 3/7')
	end

	it 'deve retornar array com lancamentos recorrentes semanais para recorrencia por parcelas' do
		recorrencia = prepara_objeto_recorrencia( 'Semanal', 7, 'on', 1, '' )
		lancamento = prepara_objeto_lancamento
		recorrencias = @lancamento_business.gera_recorrencia( lancamento, recorrencia, @user )

		data_lacamento = DateTime.new(2014,4,19,0,0,0 )
		expect( recorrencias[0].data ).to eq( data_lacamento )

		data_lacamento = DateTime.new(2014,4,26,0,0,0 )
		expect( recorrencias[1].data ).to eq( data_lacamento )

		data_lacamento = DateTime.new(2014,5,31,0,0,0 )
		expect( recorrencias[6].data ).to eq( data_lacamento )
	end

	it 'deve retornar array com lancamentos recorrentes bisemanais para recorrencia por parcelas' do
		recorrencia = prepara_objeto_recorrencia( 'Quinzenal', 15, 'on', 1, '' )
		lancamento = prepara_objeto_lancamento
		recorrencias = @lancamento_business.gera_recorrencia( lancamento, recorrencia, @user )

		data_lacamento = DateTime.new(2014,4,19,0,0,0 )
		expect( recorrencias[0].data ).to eq( data_lacamento )

		data_lacamento = DateTime.new(2014,5,3,0,0,0 )
		expect( recorrencias[1].data ).to eq( data_lacamento )

		data_lacamento = DateTime.new(2014,9,20,0,0,0 )
		expect( recorrencias[11].data ).to eq( data_lacamento )
	end

	it 'deve retornar array com 5 lancamentos recorrentes mensais para recorrencia por parcelas' do
		recorrencia = prepara_objeto_recorrencia( 'Mensal', 5, 'on', 1, '' )
		lancamento = prepara_objeto_lancamento
		recorrencias = @lancamento_business.gera_recorrencia( lancamento, recorrencia, @user )

		expect( recorrencias.length ).to eq(5)

		data_lacamento = DateTime.new(2014,4,19,0,0,0 )
		expect( recorrencias[0].data ).to eq( data_lacamento )

		data_lacamento = DateTime.new(2014,8,19,0,0,0 )
		expect( recorrencias[4].data ).to eq( data_lacamento )

		expect( recorrencias[0].descricao ).to eq('Teste de Recorrencia 1/5')
		expect( recorrencias[4].descricao ).to eq('Teste de Recorrencia 5/5')
	end

	it 'deve retornar array com lancamentos recorrentes bimestrais para recorrencia por parcelas' do
		recorrencia = prepara_objeto_recorrencia( 'Bimestral', 3, 'on', 1, '' )
		lancamento = prepara_objeto_lancamento
		recorrencias = @lancamento_business.gera_recorrencia( lancamento, recorrencia, @user )

		data_lacamento = DateTime.new(2014,4,19,0,0,0 )
		expect( recorrencias[0].data ).to eq( data_lacamento )

		data_lacamento = DateTime.new(2014,6,19,0,0,0 )
		expect( recorrencias[1].data ).to eq( data_lacamento )

		data_lacamento = DateTime.new(2014,8,19,0,0,0 )
		expect( recorrencias[2].data ).to eq( data_lacamento )
	end

	it 'deve retornar array com lancamentos recorrentes semestrais para recorrencia por parcelas' do
		recorrencia = prepara_objeto_recorrencia( 'Semestral', 7, 'on', 1, '' )
		lancamento = prepara_objeto_lancamento
		recorrencias = @lancamento_business.gera_recorrencia( lancamento, recorrencia, @user )

		data_lacamento = DateTime.new(2014,4,19,0,0,0 )
		expect( recorrencias[0].data ).to eq( data_lacamento )

		data_lacamento = DateTime.new(2015,10,19,0,0,0 )
		expect( recorrencias[3].data ).to eq( data_lacamento )

		data_lacamento = DateTime.new(2017,4,19,0,0,0 )
		expect( recorrencias[6].data ).to eq( data_lacamento )
	end

	it 'deve retornar array com lancamentos recorrentes anuais para recorrencia por parcelas' do
		recorrencia = prepara_objeto_recorrencia( 'Anual', 4, 'on', 1, '' )
		lancamento = prepara_objeto_lancamento
		recorrencias = @lancamento_business.gera_recorrencia( lancamento, recorrencia, @user )

		data_lacamento = DateTime.new(2014,4,19,0,0,0 )
		expect( recorrencias[0].data ).to eq( data_lacamento )

		data_lacamento = DateTime.new(2017,04,19,0,0,0 )
		expect( recorrencias[3].data ).to eq( data_lacamento )
	end

	it 'deve retornar array lancamentos recorrentes mensais para recorrencia por parcelas iniciado em parcela 2' do
		recorrencia = prepara_objeto_recorrencia( 'Mensal', 5, 'on', 2, '' )
		lancamento = prepara_objeto_lancamento
		recorrencias = @lancamento_business.gera_recorrencia( lancamento, recorrencia, @user )

		expect( recorrencias.length ).to eq(4)

		data_lacamento = DateTime.new(2014,4,19,0,0,0 )
		expect( recorrencias[0].data ).to eq( data_lacamento )

		expect( recorrencias[0].descricao ).to eq('Teste de Recorrencia 2/5')
	end

	it 'deve retornar array com lancamentos recorrentes diarios para recorrencia por data' do
		recorrencia = prepara_objeto_recorrencia( 'Diário', 0, 'off', 0, '25/04/2014' )
		# recorrencia.data_fim = DateTime.new(2014,4,25,0,0,0 )
		lancamento = prepara_objeto_lancamento
		recorrencias = @lancamento_business.gera_recorrencia( lancamento, recorrencia, @user )

		expect( recorrencias.length ).to eq( 7 )

		data_lacamento = DateTime.new(2014,4,19,0,0,0 )
		expect( recorrencias[0].data ).to eq( data_lacamento )

		data_lacamento = DateTime.new(2014,4,22,0,0,0 )
		expect( recorrencias[3].data ).to eq( data_lacamento )
	end

	it 'deve retornar array com lancamentos recorrentes semanais para recorrencia por data' do
		recorrencia = prepara_objeto_recorrencia( 'Semanal', 0, 'off', 0, '20/05/2014' )
		# recorrencia.data_fim = DateTime.new(2014,5,20,0,0,0 )
		lancamento = prepara_objeto_lancamento
		recorrencias = @lancamento_business.gera_recorrencia( lancamento, recorrencia, @user )

		expect( recorrencias.length ).to eq( 5 )

		data_lacamento = DateTime.new(2014,4,19,0,0,0 )
		expect( recorrencias[0].data ).to eq( data_lacamento )

		data_lacamento = DateTime.new(2014,5,17,0,0,0 )
		expect( recorrencias[4].data ).to eq( data_lacamento )
	end

	it 'deve retornar array com lancamentos recorrentes bisemanais para recorrencia por data' do
		recorrencia = prepara_objeto_recorrencia( 'Quinzenal', 0, 'off', 0, '20/05/2014' )
		# recorrencia.data_fim = DateTime.new(2014,5,20,0,0,0 )
		lancamento = prepara_objeto_lancamento
		recorrencias = @lancamento_business.gera_recorrencia( lancamento, recorrencia, @user )

		expect( recorrencias.length ).to eq( 3 )

		data_lacamento = DateTime.new(2014,4,19,0,0,0 )
		expect( recorrencias[0].data ).to eq( data_lacamento )

		data_lacamento = DateTime.new(2014,5,17,0,0,0 )
		expect( recorrencias[2].data ).to eq( data_lacamento )
	end

	it 'deve retornar array com lancamentos recorrentes mensais para recorrencia por data' do
		recorrencia = prepara_objeto_recorrencia( 'Mensal', 0, 'off', 0, '20/12/2014' )
		lancamento = prepara_objeto_lancamento
		lancamento.data = '20/08/2014'
		recorrencias = @lancamento_business.gera_recorrencia( lancamento, recorrencia, @user )

		expect( recorrencias.length ).to eq( 5 )

		data_lacamento = DateTime.new(2014,8,20,0,0,0)
		expect( recorrencias[0].data ).to eq( data_lacamento )

		data_lacamento = DateTime.new(2014,12,20,0,0,0)
		expect( recorrencias[4].data ).to eq( data_lacamento )
	end

	it 'deve retornar array com lancamentos recorrentes bimestrais para recorrencia por data' do
		recorrencia = prepara_objeto_recorrencia( 'Bimestral', 0, 'off', 0, '19/06/2014' )
		# recorrencia.data_fim = DateTime.new(2014,06,19,0,0,0 )
		lancamento = prepara_objeto_lancamento
		recorrencias = @lancamento_business.gera_recorrencia( lancamento, recorrencia, @user )

		expect( recorrencias.length ).to eq( 2 )

		data_lacamento = DateTime.new(2014,4,19,0,0,0 )
		expect( recorrencias[0].data ).to eq( data_lacamento )

		data_lacamento = DateTime.new(2014,6,19,0,0,0 )
		expect( recorrencias[1].data ).to eq( data_lacamento )
	end

	it 'deve retornar array com lancamentos recorrentes semestrais para recorrencia por data' do
		recorrencia = prepara_objeto_recorrencia( 'Semestral', 0, 'off', 0, '19/06/2016' )
		# recorrencia.data_fim = DateTime.new(2016,06,19,0,0,0 )
		lancamento = prepara_objeto_lancamento
		recorrencias = @lancamento_business.gera_recorrencia( lancamento, recorrencia, @user )

		expect( recorrencias.length ).to eq( 5 )

		data_lacamento = DateTime.new(2015,10,19,0,0,0 )
		expect( recorrencias[3].data ).to eq( data_lacamento )
	end

	it 'deve retornar array com lancamentos recorrentes anuais para recorrencia por data' do
		recorrencia = prepara_objeto_recorrencia( 'Anual', 0, 'off', 0, '18/04/2025' )
		# recorrencia.data_fim = DateTime.new(2025,04,18,0,0,0 )
		lancamento = prepara_objeto_lancamento
		recorrencias = @lancamento_business.gera_recorrencia( lancamento, recorrencia, @user )

		expect( recorrencias.length ).to eq( 11 )

		data_lacamento = DateTime.new(2023,04,19,0,0,0 )
		expect( recorrencias[9].data ).to eq( data_lacamento )
	end

	
	private

	def prepara_objeto_recorrencia( periodicidade, quantidade_parcelas, parcelado, parcela_inicial, data_final )
		recorrencia = Recorrencia.new(
			periodicidade: periodicidade, 
			parcelado: parcelado, 
			quantidade_parcelas: quantidade_parcelas, 
			parcela_inicial: parcela_inicial,
			data_fim: data_final )
		recorrencia.save!

		recorrencia
	end

	def prepara_objeto_lancamento
		lancamento = Lancamento.new
		lancamento.data = '19/04/2014'
		lancamento.descricao = 'Teste de Recorrencia'
		lancamento.valor_previsto = Currency.new.string_to_int '120,00'
		lancamento
	end
end