require 'spec_helper'

describe SaldoAtual do

	before do 
		@user = User.create!(email: 'teste@tfdsf.ts1', password: '121242534')
	end

  it "shold create SaldoAtual to user" do
    SaldoAtual.create!(valor: 10001, user: @user)

    saldos = SaldoAtual.where(:user => @user)
    expect( saldos[0].valor ).to eq(10001)
  end

  it "shold find SaldoAtual to user" do
  	SaldoAtual.create!(valor: 10000, user: @user)

  	saldos = SaldoAtual.where(:user => @user)

    expect( saldos[0].valor ).to eq(10000)
  end

  it "shold update SaldoAtual to user" do
  	SaldoAtual.create!(valor: 10000, user: @user)

  	saldos = SaldoAtual.where(:user => @user)
  	saldo = saldos[0]
  	saldo.valor = 50000
  	saldo.save!

    expect( saldo.valor ).to eq(50000)
  end


end
