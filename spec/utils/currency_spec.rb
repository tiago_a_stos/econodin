require 'spec_helper'
require 'utils/currency'

describe Currency do
	@@currency = Currency.new

	it "deve converter de string moeda para integer" do
		valor = "12,30"
		valorInt = @@currency.string_to_int(valor)
		expect( valorInt ).to eq(1230)

		valor = "0,30"
		valorInt = @@currency.string_to_int(valor)
		expect( valorInt ).to eq(30)

		valor = "30,00"
		valorInt = @@currency.string_to_int(valor)
		expect( valorInt ).to eq(3000)

		valor = "0,01"
		valorInt = @@currency.string_to_int(valor)
		expect( valorInt ).to eq(1)
	end

	it "deve converter de integer para string formatada" do
		valor = 120
		valorStr = @@currency.to_currency(valor)
		expect( valorStr ).to eq("1,20")

		valor = 12
		valorStr = @@currency.to_currency(valor)
		expect( valorStr ).to eq("0,12")

		valor = 999990
		valorStr = @@currency.to_currency(valor)
		expect( valorStr ).to eq("9999,90")

		valor = 875900
		valorStr = @@currency.to_currency(valor)
		expect( valorStr ).to eq("8759,00")

		valor = 2200
		valorStr = @@currency.to_currency(valor)
		expect( valorStr ).to eq("22,00")

		valor = 1
		valorStr = @@currency.to_currency(valor)
		expect( valorStr ).to eq("0,01")

		valor = 0
		valorStr = @@currency.to_currency(valor)
		expect( valorStr ).to eq("0,00")

		valor = 87590024664557523425534543
		valorStr = @@currency.to_currency(valor)
		expect( valorStr ).to eq("875900246645575234255345,43")

		valor = 5008
		valorStr = @@currency.to_currency(valor)
		expect( valorStr ).to eq("50,08")
	end

	it "deve converter de integer negativo para string formatada" do
		valor = -120
		valorStr = @@currency.to_currency(valor)
		expect( valorStr ).to eq("-1,20")

		valor = -12
		valorStr = @@currency.to_currency(valor)
		expect( valorStr ).to eq("-0,12")

		valor = -999990
		valorStr = @@currency.to_currency(valor)
		expect( valorStr ).to eq("-9999,90")

		valor = -875900
		valorStr = @@currency.to_currency(valor)
		expect( valorStr ).to eq("-8759,00")

		valor = -2200
		valorStr = @@currency.to_currency(valor)
		expect( valorStr ).to eq("-22,00")

		valor = -1
		valorStr = @@currency.to_currency(valor)
		expect( valorStr ).to eq("-0,01")

		valor = -0
		valorStr = @@currency.to_currency(valor)
		expect( valorStr ).to eq("0,00")

		valor = -87590024664557523425534543
		valorStr = @@currency.to_currency(valor)
		expect( valorStr ).to eq("-875900246645575234255345,43")
	end
end